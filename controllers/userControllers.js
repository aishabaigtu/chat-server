const User = require('./../models/User');
const Room = require('./../models/Room');
const bcrypt= require('bcrypt');
const auth = require('./../auth');

module.exports.register = (reqBody, res) => {
	const {userName, password} = reqBody;

	let newUser = new User({
		userName: userName,
		password: bcrypt.hashSync(password, 12)
	})

	return User.findOne({userName: userName})
	.then(result => {
		if(result == null){
			return newUser.save().then(result => {
				res.status(200).json({
            		message: "Registration successful!"
        		})
			})
		} else {
			res.status(400).json({
            		error: "Username already exists."
        	})
		}
		
	})
	.catch(err => {
		res.status(400).json({
            error: err
        })
	})
}

module.exports.login = (reqBody, res) => {
	const {userName, password} = reqBody;

	return User.findOne({userName: userName})
	.then(result => {
		if(result == null){
			res.status(400).json({
            	error: "Username doesn't exist"
        	})
		} else {
			let isPasswordRight = bcrypt.compareSync(
				password, result.password);

			if(isPasswordRight) {
				res.status(200).json({
            		access: auth.createAccessToken(result)
        		})
				
			} else {
				res.status(400).json({
            		error: "Password incorrect"
        		})
			}
		}
	})
	.catch(err => {
		res.status(400).json({
            error: err
        })
	})
}

module.exports.getContactList = (token, res) => {

	let id = auth.decode(token).id;

	return User.findById(id)
	.then(result => {
		res.status(200).json({
            contacts: result.contactList
        })
	})
	.catch(err => {
		res.status(400).json({
            error: err
        })
	})
}

module.exports.addToContactList = (userId, token, res) => {

	let id = auth.decode(token).id;

	return User.findById(id)
	.then(result => {
		let isUserOnContactList = result.contactList.some(user => user == userId);
		if(isUserOnContactList){
			res.status(400).json({
	            error: "User already on contacts"
	        })
		} else {
			result.contactList.push(userId);
			return result.save().then(result => {
				res.status(200).json({
	            	message: "Successfully added to contacts"
	        	})
			})
		}

	})
	.catch(err => {
		res.status(400).json({
            error: err
        })
	})
}

module.exports.createRoom = (userId, token, res) => {

	let id = auth.decode(token).id;

	return User.findById(id)
	.then(result => {
		let isUserInARoom = result.chatRooms.some(user => user.chatMate == userId);

		if(isUserInARoom){
			res.status(400).json({
            	error: "Room already exists"
        	})
		} else {

			let newRoom = new Room({
				userId1: id,
				userId2: userId
			})

			return newRoom.save().then(room => {

				User.findById(id)
				.then(result => {
					result.chatRooms.push({
						roomId: room._id,
						chatMate: userId
					})

					result.save();

					User.findById(userId)
					.then(result => {
						result.chatRooms.push({
							roomId: room._id,
							chatMate: id
						})

						result.save().then(result => {
							res.status(200).json({
			            		messsage: "Room successfully created"
			        		})
						})
					})
				})
				
			})
		}
	})
	.catch(err => {
		res.status(400).json({
            error: err
        })
	})
}

module.exports.getAllRooms = (roomId, token, res) => {
	let id = auth.decode(token).id;

	return User.findById(id)
	.then(result => {
		res.status(200).json({
            rooms: result.chatRooms
        })
	})
	.catch(err => {
		res.status(400).json({
            error: err
        })
	})
}

module.exports.getRoom = (roomId, res) => {

	return Room.findById(roomId)
	.then(result => {
		if(result!==null) {
			result.messages.forEach(msg => msg.isRead = true);
			result.save();

			res.status(200).json({
            	room: result
        	})
		} else {
			res.status(400).json({
            	error: "Room doesn't exist"
        	})
		}

	})
	.catch(err => {
		res.status(400).json({
            error: err
        })
	})
}

module.exports.message = (roomId, token, reqBody, res) => {
	let id = auth.decode(token).id;
	let {message} = reqBody;

	return Room.findById(roomId)
	.then(result => {
		result.messages.push({
			message: message,
			senderId: id
		})

		return result.save().then(result => {
			res.status(200).json({
            	messsage: "Message sent"
        	})
		})
	})
	.catch(err => {
		res.status(400).json({
            error: err
        })
	})
}