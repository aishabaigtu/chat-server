const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	userName: {type: String, unique: true},
	password: {type: String, required: true},
	contactList: [
		{type: mongoose.Schema.Types.ObjectId, ref: 'User'}
	],
	chatRooms: [{
		roomId:  {type: mongoose.Schema.Types.ObjectId, ref: 'Room'},
		chatMate: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
	}]
})

module.exports = mongoose.model("User", userSchema);