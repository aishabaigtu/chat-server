const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({

	userId1: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
	userId2: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
	messages: [{
		message: {type: String, required: true},
		senderId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
		date: {type: Date, default: new Date()},
		isRead: {type: Boolean, default: false}
	}]
})

module.exports = mongoose.model("Room", roomSchema);