
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const PORT = process.env.PORT || 4000;
const cors = require('cors');
const auth = require('./auth');

const userControllers = require('./controllers/userControllers');

mongoose.connect('mongodb+srv://admin:admin-1996%2F%2A@batch139.ppmgo.mongodb.net/chatApp?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//routes
app.post('/register', (req, res) => {
	userControllers.register(req.body, res);
})

app.post('/login', (req, res) => {
	userControllers.login(req.body, res);
})

app.get('/contacts', auth.userVerify, (req, res) => {
	userControllers.getContactList(req.headers.authorization, res);
})

app.put('/:userId/add-contact', auth.userVerify, (req, res) => {
	userControllers.addToContactList(req.params.userId, req.headers.authorization, res);
})

app.post('/:userId/create-room', auth.userVerify, (req, res) => {
	userControllers.createRoom(req.params.userId, req.headers.authorization, res);
})

app.get('/rooms', auth.userVerify, (req, res) => {
	userControllers.getAllRooms(req.params.roomId, req.headers.authorization, res);
})

app.get('/:roomId/room', auth.userVerify, (req, res) => {
	userControllers.getRoom(req.params.roomId, res);
})

app.post('/:roomId/message', auth.userVerify, (req, res) => {
	userControllers.message(req.params.roomId, req.headers.authorization, req.body, res);
})


app.listen(PORT, () => console.log(`Server is running at port ${PORT}`))