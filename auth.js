
const jwt = require('jsonwebtoken');
const secret = "chatApp";

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		userName: user.userName
	}

	return jwt.sign(data, secret, {});
}

module.exports.userVerify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") {

		token = token.slice(7);

		return jwt.verify(token, secret, (error, result) => {
			
			if(error) {
				return res.send(false);
			} else {
				return next();
			}
		})
	}
}

module.exports.decode = (token) => {

	if(typeof token !== "undefined") {

		token = token.slice(7);

		return jwt.verify(token, secret, (error, result) =>
			error ? null : jwt.decode(token, {complete: true}).payload)
	}
}